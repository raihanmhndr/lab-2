// Bagian Chat
var chatHead = document.getElementsByClassName('chat-head');
var chatBody = document.getElementsByClassName('chat-body');
var toggleChat = true;

$(chatHead).click(function(){
    $(chatBody).toggle();
});

$("#text").keypress(function(event) {
	/* Jika mencet enter tanpa shift do chat*/
	if (event.keyCode == 13 && !event.shiftKey){
		event.preventDefault();
        var txt = $("#text").val();

        // Kalo jadi pengirim (orang A)
		if (toggleChat) {
			$(".msg-insert").append("<p class='msg-send'>"+txt+"</p>");
			toggleChat = false;
        }
        
        // Kalo jadi penerima (orang B)
        else {
			$(".msg-insert").append("<p class='msg-receive'>"+txt+"</p>");
			toggleChat = true;
        }
        
		$(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast');
		$("#text").val("");
		chatBot(txt);
		return;
	}
});
// END

// Bagian Kalkulator
var print = document.getElementById('print');

var go = function(x) {
    if (x === 'ac') {
        /* implemetnasi clear all */
        print.value = '';
    } 
        
    else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
    } 
    
    else if (x === 'log'|| x === 'sin'|| x === 'tan'){
        /* implementasi log sin dan tan */
        switch(x) {
            case 'log': print.value = Math.log10(print.value);
            break;
            case 'sin': print.value = Math.sin(print.value);
            break;
            case 'tan': print.value = Math.tan(print.value);
            break;
        }
    }
    
    else {
        print.value += x;
    }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

// Bagian Select2
var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#212121"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#212121"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#212121"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#212121"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#212121"}
];

//masukin semua theme ke local storage
localStorage.setItem("themes",JSON.stringify(themes));
var catalog = JSON.parse(localStorage.getItem("themes"));

// Ketika web dibuka dan belum pernah set warna
if (localStorage.getItem('selectedTheme') === null) { 
    localStorage.setItem('selectedTheme', JSON.stringify(catalog[3])); 
}
var theme = JSON.parse(localStorage.getItem('selectedTheme'));
changeTheme(theme);

function changeTheme(x){
    $('body').css({"backgroundColor": x['bcgColor']});
    $('.text-center').css({"color": x['fontColor']});
}

$(document).ready(function() {
    $('.my-select').select2({'data' : themes}).val(theme['id']).change();
    $('.apply-button').on('click', function(){
        theme = themes[$('.my-select').val()];
        changeTheme(theme);
        localStorage.setItem('selectedTheme',JSON.stringify(theme));
    })
});
// END
